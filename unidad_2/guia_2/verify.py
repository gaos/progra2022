import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from dialog import Dialog
from in_forum import In_Forum
class Verify(Gtk.Window):
    """docstring for verify, se encarga de verificar abriendo el archivo y
    buscando en el usuario y marca quien estra trabajando una vez dentro."""

    def __init__(self, parent):
        super().__init__(title="Ingresar usuario", transient_for=parent)
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=4,)
        self.add(box)
        self.resize(350, 350)

        self.__label = Gtk.Label("Por favor Ingrese sus datos\n Usuario:")
        box.add(self.__label)

        self.entrada = Gtk.Entry()
        box.add(self.entrada)

        self.__label2 = Gtk.Label("Contraseña:")
        box.add(self.__label2)

        self.entrada2 = Gtk.Entry()
        box.add(self.entrada2)

        self.button1 = Gtk.Button(label="Ok")
        self.button1.connect("clicked", self.seeing)
        box.add(self.button1)



        self.show_all()

    def seeing(self,widget):
        flag = False
        file_user = open("quien_esta.txt", "w")
        file_user.write(self.entrada.get_text())
        file_user.close()

        file = open("user.duck","r")
        dictionary = (file.read())

        # para crear un diccionario con los inscritos en .duck, el guion
        # para separar
        Dict = dict((x.strip(), y.strip())
             for x, y in (element.split(':')
             for element in dictionary.split('-')))
        try:
            if Dict[self.entrada.get_text()] == self.entrada2.get_text():
                flag = True
                foro = In_Forum(self)
                foro.connect("destroy", Gtk.main_quit)
                foro.show_all()
                Gtk.main()
            else:
                bad = Dialog(self)
                response = bad.run()
                bad.destroy()
        except:
            bad = Dialog(self)
            response = bad.run()
            bad.destroy()
        file.close()
