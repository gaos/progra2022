import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Dialog(Gtk.Dialog):
# se encarga de dar inicio a la ventana de dialogo, dandole asi, un boton ok
#Se hereda del padre (transient_for)

    def __init__(self, parent):
        """
        por si ingresa datos erroneos
        """
        super().__init__(title="Ingreson't", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)
        label = Gtk.Label(label="¡¡¡Datos erroneos!!!")
        box = self.get_content_area()
        box.add(label)
        self.show_all()
