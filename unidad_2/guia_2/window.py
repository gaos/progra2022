import os
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from make_user import Make_user
from verify import Verify

def guardar_direccion(direccion, numero):
    #1 archivo lugar
    if numero == 1:
        file = open("direccion", "w")
        file.write(direccion)
        file.close()
    #2 carpeta lugar
    elif numero == 2:
        file = open("direccion2", "w")
        file.write(direccion)
        file.close()
"""
Funcion que se encarga de los archivos y ver su contenido
"""
def open_file(numero):
    #1 lugar del archivo
    if numero == 1:
        file = open("direccion","r")
        lugar = file.read()
        file.close()
    #2 lugar de la carpeta
    elif numero == 2:
        file = open("direccion2","r")
        lugar = file.read()
        file.close()
    return lugar

class Window(Gtk.Window):
    """docstring for Window."""

    def __init__(self):
        Gtk.Window.__init__(self, title="Foros")
        # caja vertical
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=4)
        self.resize(350, 350)
        self.add(box)

        self.__label = Gtk.Label("Bienvenido a Foros cutres, por favor escoja")
        box.add(self.__label)
        # boton para escoger una carpeta/archivo, dependiendo de lo escogido
        # se hará en el futuro para que pueda ver los foros


        button2 = Gtk.Button(label="Crear usuario")
        button2.connect("clicked", self.on_user)
        box.add(button2)

        button3 = Gtk.Button(label="Login/Ingresar")
        button3.connect("clicked", self.its_this_a_user)
        box.add(button3)


    def on_user(self, widget):
        window_user = Make_user(self)
        window_user.connect("destroy", Gtk.main_quit)
        window_user.show_all()
        Gtk.main()

    def its_this_a_user(self, widget):
        window_user = Verify(self)
        window_user.connect("destroy", Gtk.main_quit)
        window_user.show_all()
        Gtk.main()


    def add_filters(self, dialog):
        filter = Gtk.FileFilter()
        filter.set_name("md")
        filter.add_pattern("*.md")
        dialog.add_filter(filter)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Todo")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)
