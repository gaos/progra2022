# Guía 2 - Unidad II





El programa fue escrito en lenguaje de programación Python, y consiste en realizar una aplicación grafica para resolver un problema sencillo, para ello se debe tener en cuenta los siguientes conceptos:

* POO

* API

* Clases

* Funciones y Metodos







## Historia





Escrito y desarrollado por el estudiante de Ing. Civil en Bioinformática Gabriel Rojas Hormazábal y Cristobal Briceño Arellano, utilizando para ello el lenguaje de programación Python.







## Para empezar





Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas.









### Pre-requisitos





Es requisito cerciorarse de tener instalado Python y su compilador correspondiente en el equipo donde ejecutará el proyecto.

El siguiente paso antes de iniciar la instalacion y ejecucion del progra es configurar PyGObject y sus dependencias, para ello existen dos opciones. La primera es instalar el paquete desde el repositorio oficial (PyGObject), casi todas las principales distribuciones de Linux empaquetan versiones recientes de PyGObject. La segunda es instalar PyGObject desde la fuente, usando JHBuild, este está diseñado para compilar fácilmente paquetes fuente y descubrir qué dependencias se deben compilar y en qué orden. Para configurar JHBuild, siga el manual de JHBuild de su repositorio oficial.









### Instalación y ejecución



Para iniciar el programa es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones), posteriormente se debe instalar “git” directamente desde la terminal de su ordenador Linux con el comando “sudo apt install git”. Luego de esto, uno debe proceder a crear su cuenta de GitLab, en caso de ya tener una omitir este paso. Lo siguiente es dirigirse al repositorio < https://gitlab.com/gaos/progra2022.git > y presionar la opción “Clone” seleccionando el formato https para copiarlo. Después uno debe dirigirse a la terminal y escribir el comando “git clone < https://gitlab.com/gaos/progra2022.git >, para finalizar escribir el comando “cd unidad_2/guia_2/” y seguido “python3 start.py”. Tras haber seguido estos pasos deberías poder interactuar con el programa sin mayor problema. Puede subir al repositorio esto para desde gitlab visualizar el archivo markdown en línea.







## Funcionamiento

En la ventana principal tenemos el foro generado por Gtk (Gtk 3.0, siendo esta window.py), con dos botones y un label, dependiendo del boton seleccionado; en el caso de presionar crear usuario (window, make_user.py) el usuario podrá crear su usuario con usuario y contraseña en esta ventana (usando Gtk.Entry) guardando así la creación, siendo posible la creacion de multiples usuarios. Luego de crear el usuario está el botón [Login/Ingresar] (verify.py), este tiene una ventana dialog (dialog.py) para el caso en que se equivoque de contraseña o no este registrado ese usuario, en verify tiene la opción de ingresar utilizando Gtk.Entry y un boton [Ok]. Aqui se genera una lista de botones siendo dependiente de la cantidad de carpetas en la carpeta foros, sea cual sea la ingresada se genera otra ventana (writting_on_f.py) la cual tiene dos botones un boton [nuevo foro] directo a crear un md con el usuario o sobre escribir el del usuario en el caso de que ya este hecho el archivo markdown con una ventana de texto (escribir.py). El otro botón [Editar textos] generará un FileChooser con posibilidad de filtrar archivos [.md] de esta forma se selecciona el archivo que editar yendo a una ventana de texto (escribir.py). En escribir tiene el botón guardar que debe ser presionado para terminar de guardar lo escrito. Dando la posibilidad al usuario de volver a editar textos. Y para finalizar debe cerrar cada ventana utilizada.





## Codificación





Soporta la codificación estándar UTF-8.









## Construido con





* [Visual Studio Code.](https://code.visualstudio.com) y [Atom.](https://atom.io) - IDE utilizado para el desarrollo del proyecto.









## Autores





* Gabriel Rojas Hormazábal.

* Cristóbal Briceño Arellano.
