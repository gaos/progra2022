import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import os
from escribir import TextViewWindow

def guardar_direccion(direccion, numero):
    #1 archivo lugar
    if numero == 1:
        file = open("direccion", "w")
        file.write(direccion)
        file.close()


class Writting_on(Gtk.Window):
    """docstring for writting on, se encarga de llevar a editar textos, esta
    con escribir para poder escribir los textos y editarlos dependiendo del
    boton."""

    def __init__(self, parent, lugar):
        self.__lugar = lugar
        super().__init__(transient_for=parent)

        headerbar = Gtk.HeaderBar()
        headerbar.set_title("Foros")
        headerbar.set_subtitle("{0}".format(lugar))
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)

        button = Gtk.Button("¿?")
        button.connect("clicked", self.about_cb)
        headerbar.pack_start(button)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=4,)
        self.add(box)
        self.resize(350, 350)

        file = open("quien_esta.txt", "r")
        nombre = file.read()
        file.close()

        self.__label = Gtk.Label("Bienvenido {0} a el foro de {1}:".format(
            nombre, lugar))
        box.add(self.__label)


        liststore = Gtk.ListStore(str)

        pwd = os.popen("pwd")
        carpeta = pwd.read()
        list_carpeta = os.popen("ls /{0}/foros/{1}".format(carpeta.strip(), lugar))
        for i in list_carpeta:
            liststore.append([i])




        treeview = Gtk.TreeView()
        treeview.set_model(liststore)

        cellrenderertext = Gtk.CellRendererText()

        treeviewcolumn = Gtk.TreeViewColumn("Archivos")
        treeview.append_column(treeviewcolumn)
        treeviewcolumn.pack_start(cellrenderertext, True)
        treeviewcolumn.add_attribute(cellrenderertext, "text", 0)

        box.add(treeview)

        self.button1 = Gtk.Button(label="Editar textos")
        self.button1.connect("clicked", self.on_file_clicked)
        box.add(self.button1)

        self.button2 = Gtk.Button(label="nuevo foro")
        self.button2.connect("clicked", self.directo)
        box.add(self.button2)


        self.show_all()

    def about_cb(self, widget):

        aboutdialog = Gtk.AboutDialog()


        authors = ["Gabriel Rojas", "Cristobal Briceños"]
        documenters = ["Foros"]


        aboutdialog.set_program_name("Acerca de los autores")
        aboutdialog.set_copyright(
            "Copyright \xc2\xa9 2020 Gaos_180")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("https://gitlab.com/gaos/progra2022/-/tree/main/unidad_2")
        aboutdialog.set_website_label("Repositorio")

        aboutdialog.set_title("Acerca de nosotros")

        aboutdialog.connect("response", self.on_close)
        aboutdialog.show()

    def on_close(self, action, parameter):
        action.destroy()

    # directo al foro del usuario
    def directo(self,widget):
        # para obtener la direccion a crear
        pwd = os.popen("pwd")
        carpeta = pwd.read()
        file = open("quien_esta.txt", "r")
        nombre = file.read()
        file.close()

        # Crea el archivo md con el nombre del usuario
        file_new = open("{0}{1}.md".format(carpeta.strip() + "/foros"+"/"+self.__lugar +"/", nombre) , "w")
        guardar_direccion("{0}{1}.md".format(carpeta.strip() + "/foros"+"/"+self.__lugar +"/", nombre),1)
        file_new.close()

        win = TextViewWindow()
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()

    def on_file_clicked(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Pls elige", parent=self, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            nombre_md = dialog.get_filename()
            guardar_direccion(nombre_md, 1)

            win = TextViewWindow()
            win.connect("destroy", Gtk.main_quit)
            win.show_all()
            Gtk.main()

        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def add_filters(self, dialog):
        filter = Gtk.FileFilter()
        filter.set_name("md")
        filter.add_pattern("*.md")
        dialog.add_filter(filter)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Todo")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)
