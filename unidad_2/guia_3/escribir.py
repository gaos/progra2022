import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango

def open_file(numero):
    #1 lugar del archivo
    if numero == 1:
        file = open("direccion","r")
        lugar = file.read()
        file.close()


class TextViewWindow(Gtk.Window):
    """
    En escribir se hace la ventana de texto para escribir, este luego es guardado
    """
    def __init__(self):
        Gtk.Window.__init__(self)


        headerbar = Gtk.HeaderBar()
        headerbar.set_title("Foros")
        headerbar.set_subtitle("Escribiendo")
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)

        self.set_default_size(720, 480)

        self.grid = Gtk.Grid()
        self.add(self.grid)

        self.create_textview()

    def create_textview(self):
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)

        lugar = open_file(1)
        try:
            file = open("{0}".format(lugar), "r")
        except:
            file = open("{0}".format(lugar), "w")
            file.close()
            file = open("{0}".format(lugar), "r")

        texto = file.read()
        file.close()
        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()
        self.textbuffer.set_text(texto)


        scrolledwindow.add(self.textview)
        button1 = Gtk.Button(label="Guardar")
        button1.connect("clicked", self.on_clicked)
        self.grid.attach(button1, 0, 3, 3, 1)


    def about_cb(self, widget):

        aboutdialog = Gtk.AboutDialog()


        authors = ["Gabriel Rojas", "Cristobal Briceños"]
        documenters = ["Foros"]


        aboutdialog.set_program_name("Acerca de los autores")
        aboutdialog.set_copyright(
            "Copyright \xc2\xa9 2020 Gaos_180")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("https://gitlab.com/gaos/progra2022/-/tree/main/unidad_2")
        aboutdialog.set_website_label("Repositorio")

        aboutdialog.set_title("Acerca de nosotros")

        aboutdialog.connect("response", self.on_close)
        aboutdialog.show()

    def on_close(self, action, parameter):
        action.destroy()

    def on_clicked(self, widget):

        # se encarga de realizar el inicio de la parte a iterar
        start_iter = self.textbuffer.get_start_iter()

        # se encarga del final a iterar
        end_iter = self.textbuffer.get_end_iter()
        text = self.textbuffer.get_text(start_iter, end_iter, True)

        # se hace la sobre escritura del documento
        lugar = open_file(1)
        print(lugar)
        file = open("{0}".format(lugar), "w")
        file.write(text)
        file.close()
