import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
class Make_user(Gtk.Window):
    """docstring for Make_user, se crea el usuario."""

    def __init__(self, parent):
        super().__init__(transient_for=parent)

        headerbar = Gtk.HeaderBar()
        headerbar.set_title("Foros")
        headerbar.set_subtitle("Crear un usuario")
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)

        button = Gtk.Button("¿?")
        button.connect("clicked", self.about_cb)
        headerbar.pack_start(button)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=4)
        self.add(box)
        self.resize(350, 350)

        # se hace el titulo
        self.__label = Gtk.Label("Por favor cree su usuario\n\n \t\tUsuario:")
        box.add(self.__label)

        # para recibir texto usuario
        self.entrada = Gtk.Entry()
        box.add(self.entrada)

        # similar a usuario para contraseña
        self.__label2 = Gtk.Label("Contraseña:")
        box.add(self.__label2)

        self.entrada2 = Gtk.Entry()
        box.add(self.entrada2)

        self.button1 = Gtk.Button(label="Ok")
        self.button1.connect("clicked", self.make)
        box.add(self.button1)



        self.show_all()

    def about_cb(self, widget):

        aboutdialog = Gtk.AboutDialog()


        authors = ["Gabriel Rojas", "Cristobal Briceños"]
        documenters = ["Foros"]


        aboutdialog.set_program_name("Acerca de los autores")
        aboutdialog.set_copyright(
            "Copyright \xc2\xa9 2020 Gaos_180")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("https://gitlab.com/gaos/progra2022/-/tree/main/unidad_2")
        aboutdialog.set_website_label("Repositorio")

        aboutdialog.set_title("Acerca de nosotros")

        aboutdialog.connect("response", self.on_close)
        aboutdialog.show()

    def on_close(self, action, parameter):
        action.destroy()

    def make(self,widget):
        file = open("user.duck", "a")
        file.write("-" + (self.entrada.get_text() + ":" + self.entrada2.get_text()))
        file.close()
        print("Agregando")
