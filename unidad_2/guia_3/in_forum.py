import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import os
from writting_on_f import Writting_on
class In_Forum(Gtk.Window):
    """docstring for in forum, aqui se hacen las lista de foros disponibles
    dependiendo de las carpetas."""

    def __init__(self, parent):
        super().__init__(transient_for=parent)

        headerbar = Gtk.HeaderBar()
        headerbar.set_title("Foros")
        headerbar.set_subtitle("Listado")
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)


        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=4,)
        self.add(box)
        self.resize(640, 480)
        self.__label = Gtk.Label("Lista de foros\n")
        box.add(self.__label)

        """
        pwd se encarga de obtener las carpetas en foros, str lo obtiene legible
        luego la lista se hace una con split separando con \n, quedá uno extra
        asi que se borra.
        """
        pwd = os.popen("ls foros/")
        str_fors = pwd.read()
        lista_foros = str_fors.split("\n")
        self.__nombres = []

        del lista_foros[(len(lista_foros)-1)]

        for i in range(len(lista_foros)):
            self.__nombres.append(lista_foros[i])

        self.__lista = lista_foros

        for i in range(len(lista_foros)):
            lista_foros[i] = Gtk.Button(label="Escoger {0}".format(lista_foros[i]))
            lista_foros[i].connect("clicked", self.on_clicked)
            box.add(lista_foros[i])

        self.show_all()


    def about_cb(self, widget):

        aboutdialog = Gtk.AboutDialog()


        authors = ["Gabriel Rojas", "Cristobal Briceños"]
        documenters = ["Foros"]


        aboutdialog.set_program_name("Acerca de los autores")
        aboutdialog.set_copyright(
            "Copyright \xc2\xa9 2020 Gaos_180")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("https://gitlab.com/gaos/progra2022/-/tree/main/unidad_2")
        aboutdialog.set_website_label("Repositorio")

        aboutdialog.set_title("Acerca de nosotros")

        aboutdialog.connect("response", self.on_close)
        aboutdialog.show()

    def on_close(self, action, parameter):
        action.destroy()


    def on_clicked(self, widget):
        for i in range(len(self.__lista)):
            if self.__lista[i] == widget:
                foro = Writting_on(self, self.__nombres[i])
                foro.connect("destroy", Gtk.main_quit)
                foro.show_all()
                Gtk.main()
