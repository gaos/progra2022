import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Dialog(Gtk.Dialog):
# se encarga de dar inicio a la ventana de dialogo, dandole asi, un boton ok
#Se hereda del padre (transient_for)

    def __init__(self, parent):
        """
        por si ingresa datos erroneos
        """
        super().__init__(transient_for=parent, flags=0)

        headerbar = Gtk.HeaderBar()
        headerbar.set_title("Foros")
        headerbar.set_subtitle("Ingreso denegado")
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)

        button = Gtk.Button("¿?")
        button.connect("clicked", self.about_cb)
        headerbar.pack_start(button)

        self.add_buttons(
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(300, 100)
        label = Gtk.Label(label="¡¡¡Datos erroneos!!!")
        box = self.get_content_area()
        box.add(label)
        self.show_all()

    def about_cb(self, widget):

        aboutdialog = Gtk.AboutDialog()


        authors = ["Gabriel Rojas", "Cristobal Briceños"]
        documenters = ["Foros"]


        aboutdialog.set_program_name("Acerca de los autores")
        aboutdialog.set_copyright(
            "Copyright \xc2\xa9 2020 Gaos_180")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("https://gitlab.com/gaos/progra2022/-/tree/main/unidad_2")
        aboutdialog.set_website_label("Repositorio")

        aboutdialog.set_title("Acerca de nosotros")

        aboutdialog.connect("response", self.on_close)
        aboutdialog.show()

    def on_close(self, action, parameter):
        action.destroy()
