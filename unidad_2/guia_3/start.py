#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import os
from window import Window


def main():
    # se encarga de iniciar

    # ventana principal para la creación de menú
    win = Window()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()


if __name__ == '__main__':
    main()
