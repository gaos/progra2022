#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from blackjack import BlackJack
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from dialog import PopUp
class Window(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="BlackJackdigans")
        self.set_default_size(400,300)
        # tamaño de la ventana
        self.set_border_width(100)
        self.__black = BlackJack()

        self.__black.sacar_carta()
        self.__black.sacar_carta()

        self.__black.bot_carta()
        self.__black.bot_carta()


        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(box)
        # homo hace que se mantenga todo el tiempo a una cantidad

        self.__label = Gtk.Label("Bienvenido a BlackJack, escoja")
        box.add(self.__label)



        button1 = Gtk.Button(label="Sacar carta")
        button1.connect("clicked", self.click1)
        box.add(button1)


        self.__button2 = Gtk.Button(label="Muestra cartas")
        self.__button2.connect("clicked", self.click3)
        box.add(self.__button2)

        self.__button3 = Gtk.Button(label="Sacar carta bot")
        self.__button3.connect("clicked", self.click2)
        box.add(self.__button3)




    def click1(self, widget):
        self.__black.sacar_carta()

    def click2(self, widget):
        self.__black.bot_carta()

    def click3(self, widget):
        dialog = PopUp(self, self.__black)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.__black.winner(self)
        dialog.destroy()
