import random
from win import Win
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class BlackJack():
    """docstring for BlackJack."""

    def __init__(self):
        self.__player = []
        self.__bot = []
        self.__cont_player = 0
        self.__cont_bot = 0

    def sacar_carta(self):
        r0 = random.randint(0,12)+1
        if(r0==13):
            print("K")
        elif(r0==12):
            print("Q")
        elif(r0==11):
            print("J")
        elif(r0==1):
            print("A")
        else:
            print(r0)
        self.__player.append(r0)

    def bot_carta(self):
        r0 = random.randint(0,12)+1
        if(r0==13):
            print("K")
        elif(r0==12):
            print("Q")
        elif(r0==11):
            print("J")
        elif(r0==1):
            print("A")
        else:
            print(r0)
        self.__bot.append(r0)

    def conteo_bot(self):
        self.__cont_bot = 0
        for i in self.__bot:
            if(i==13 or i==12 or i==11):
                self.__cont_bot = self.__cont_bot + 10
            elif(i==1):
                self.__cont_bot = self.__cont_bot + i
            else:
                self.__cont_bot = self.__cont_bot + i
        return self.__cont_bot

    def conteo_player(self):
        self.__cont_player = 0
        for i in self.__player:
            if(i==13 or i==12 or i==11):
                self.__cont_player = self.__cont_player + 10
            elif(i==1):
                self.__cont_player = self.__cont_player + i
            else:
                self.__cont_player = self.__cont_player + i
        return self.__cont_player

    def winner(self, parent):
        if(self.__cont_bot <22 and self.__cont_player < 22
           and self.__cont_bot > self.__cont_player):

            txt = "bot"
            flag = True

        elif(self.__cont_bot <22 and self.__cont_player < 22
           and self.__cont_bot < self.__cont_player):

            txt = "jugador"
            flag = True

        elif(self.__cont_bot <22 and self.__cont_player < 22
           and self.__cont_bot == self.__cont_player):

            txt = "no hay"
            flag = True
        elif(self.__cont_bot >=22 and self.__cont_player >= 22):
            txt = "Perdieron"
            flag = False
        elif(self.__cont_bot >=22 and self.__cont_player < 22):

            txt = "jugador"
            flag = True
        elif(self.__cont_bot < 22 and self.__cont_player >= 22):

            txt = "bot"
            flag = True

        print("player:", self.__player,"\nBot:", self.__bot)
        gana = Win(parent, txt, flag)
        response = gana.run()
        gana.destroy()


    @property
    def carta_player(self):
        return self.__player
    @property
    def carta_bot(self):
        return self.__bot
