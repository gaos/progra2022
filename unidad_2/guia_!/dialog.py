#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class PopUp(Gtk.Dialog):
    """docstring for PopUp."""

    def __init__(self, papi, juego):
        super().__init__(title="Cartas", transient_for=papi, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )
        if(juego.carta_bot[0] <11):
            label = Gtk.Label(label=(
                "La suma observada en la otra mano es: [] +",
                 (juego.conteo_bot() - juego.carta_bot[0])))
            box = self.get_content_area()
            box.add(label)
        else:
            label = Gtk.Label(label=("La suma observada en la otra mano es:",
                                      (juego.conteo_bot() - 10)))
            box = self.get_content_area()
            box.add(label)

        self.set_default_size(150, 100)
        if len(juego.carta_player) == 0 :
            label = Gtk.Label(label="No ha sacado carta")
            box = self.get_content_area()
            box.add(label)
        for i in juego.carta_player:
            if(i==13):
                label = Gtk.Label(label="K")
                box = self.get_content_area()
                box.add(label)
            elif(i==12):
                label = Gtk.Label(label="Q")
                box = self.get_content_area()
                box.add(label)
            elif(i==11):
                label = Gtk.Label(label="J")
                box = self.get_content_area()
                box.add(label)
            elif(i==1):
                box = self.get_content_area()
                label = Gtk.Label(label="A")
                box.add(label)
            else:
                label = Gtk.Label(label=i)
                box = self.get_content_area()
                box.add(label)


        label = Gtk.Label(label=("La suma es",juego.conteo_player()))
        box = self.get_content_area()
        box.add(label)

        label = Gtk.Label(label="¿Desea comparar cartas y ver ganador?")
        box = self.get_content_area()
        box.add(label)
        self.show_all()
