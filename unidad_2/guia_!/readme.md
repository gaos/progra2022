# **Blackjackdigans**

## Introducción
>Una versión simple del Blackjack con regla más simple y sin inteligencia artificial para el aprendizaje del blackjack

## Desarrollo

El desarrollo es mediante Gtk usando Windows y dialog para la creación de este, este permite la interacción, su pantalla principal es una simple ventana con 3 botones y un label. El label es para la introducción, mientras que sacar carta y sacar cartas bot son relacionada al objeto, el objeto se llama blackjack dónde es la interacción del juego.

Mediante el uso de mostrar cartas se hace una herencia para dialog y de esta forma poder interactuar, blackjack está asociado a este dialog para poder declarar el ganador, el cual cuando se define que desea ver los resultados se muestra quien gana mediante dialog presente en blackjack, fue hecho de esta forma para que blackjack tenga la función ganador en vez de el dialog de mostrar cartas.


## Para concluir

Una vez definido el ganador se genera un dialog para terminar juego o reiniciarlo, este dialog no está asociado a nada y es solo hecho para definir cuándo terminar o si volver a jugar
