import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Continue(Gtk.Dialog):
    def __init__(self):
        super().__init__(title="¿Continuar?")
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)

        label = Gtk.Label(label="¿Acepta volver a jugar?")

        box = self.get_content_area()
        box.add(label)
        self.show_all()
