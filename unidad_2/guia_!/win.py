import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Win(Gtk.Dialog):
    def __init__(self, parent, txt, flag):
        super().__init__(title=txt, transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)
        if flag == True:
            label = Gtk.Label(label=("El ganador es: " + txt))
        else:
            label = Gtk.Label(label=("Ambos pierde:"))

        box = self.get_content_area()
        box.add(label)
        self.show_all()
