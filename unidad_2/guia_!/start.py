#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from window import Window

from seguir import Continue

def main():
#se encarga de iniciar
    red_flag = True
    while red_flag:
        win = Window()
        win.connect("destroy", Gtk.main_quit)
        win.show_all()
        Gtk.main()
        continuar = Continue()
        response = continuar.run()
        if response == Gtk.ResponseType.OK:
            red_flag = True
        elif response == Gtk.ResponseType.CANCEL:
            red_flag = False
        continuar.connect("destroy", Gtk.main_quit)
        continuar.show_all()
        if red_flag == True:
            continuar.destroy()


if __name__ == '__main__':
    main()
