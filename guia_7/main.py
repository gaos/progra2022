from play import Play
import time
# la interfaz es basicamente un listado de la cantidad de metodos que se ocupan

# para colores, teniendo pip su comando es pip install termcolor
from termcolor import colored

def main():
    # juego es donde se desarrolla la mayoría
    juego = Play()
    personaje  = juego.Choose()
    enemigo = juego.Choose_enemy()
    juego.hacer_matriz()
    while True:
        print("Eres un:")
        personaje.toString()
        print("Y el enemigo es:")
        enemigo.toString()
        time.sleep(3)
        print("Atacaras")
        juego.Attack(personaje, enemigo)
        time.sleep(3)
        juego.animation(personaje)
        juego.reiniciar_matriz()
        personaje.toString()
        print("Enemigo:")
        enemigo.toString()
        opc = str(input("Deseas continuar atacando? S= Si:\n"))
        if opc.upper()=="S":
            print("Continua")
        else:
            print("Adios")
            break

if __name__ == '__main__':
    main()
