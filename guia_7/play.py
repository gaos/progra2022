# Clase interfaz del juego (Solo metodos de juego)
import random
import time
import os
from character import Character
from wizard import Wizard
from warrior import Warrior
from interface_play import Interface
from multipledispatch import dispatch
from termcolor import colored
class Play(Interface):
    """docstring for Play."""

    def __init__(self):
        super().__init__()
        self.__matriz = []
        pass

    def hacer_matriz(self):
        tamano = 9
        for i in range(tamano):
            self.__matriz.append(["⬛"]*tamano)
    def reiniciar_matriz(self):
        for i in range(len(self.__matriz)):
            for j in range(len(self.__matriz)):
                    self.__matriz[i][j] = "⬛"

    # para escoger
    def Choose(self):
        print("Escoja su tipo de personaje")
        print("1. Guerrero\n2. Mago")
        opc = str(input(""))
        if opc == "1":
            personaje = Warrior()
        elif opc == "2":
            personaje = Wizard()
        else:
            print("No escogio bien")
        return personaje

    def Choose_enemy(self):
        r0 = random.randint(0,1)
        if r0 == 0:
            enemy = Warrior()
        else:
            enemy = Wizard()
        return enemy

    """dispatch añadido para implementar sobrecarga, en caso de probar con otros
     tipos de variables, ahora solo funciona con tipo Guerrero y mago
     el ataque. Aunque fuera de eso por ahora no tiene utilidad asi que la
    comentaré, mas abajo """

    #@dispatch(Wizard, Wizard)
    def Attack(self, personaje, enemy):
        r0 = random.randint(0,1)
        if r0 == 0 and type(enemy) is Wizard:
            print("El enemigo preparo un hechizo")
            enemy.prepareSpell()
        if type(personaje) is Wizard:
            opc = input("¿preparas hechizo?\nS=Si\n")
            if opc.upper() == "S":
                personaje.prepareSpell()
            else:
                personaje.reinicio()
        personaje.damagePoints(enemy)
        print("El enemigo ahora tiene:", enemy.Life)
        if type(enemy) is Wizard:
            enemy.reinicio()

    # dispatch implementado para la animacion del personaje al atacar
    @dispatch(Warrior)
    def animation(self, personaje):
        for r in range(10):
            print("Guerrero atacando:")
            for i in range(len(self.__matriz)):
                for j in range(len(self.__matriz)):
                    if r == j and r == i:
                        self.__matriz[i][j] = "▓▓"
                    print(colored(self.__matriz[i][j], "red"), end='')
                print('')
            time.sleep(0.08)
            os.system("clear")
        for r in range(10):
            print("Guerrero atacando:")
            for i in range(len(self.__matriz)):
                for j in range(len(self.__matriz)):
                    if 10-r == i:
                        self.__matriz[i][j] = "▓▓"
                    print(colored(self.__matriz[i][j], "red"), end='')
                print('')
            time.sleep(0.08)
            os.system("clear")


    @dispatch(Wizard)
    def animation(self, personaje):
        for r in range(10):
            print("Mago atacando")
            for i in range(len(self.__matriz)):
                for j in range(len(self.__matriz)):
                    if r == j or r == i:
                        self.__matriz[i][j] = "▓▓"
                    print(colored(self.__matriz[i][j], "blue"), end='')
                print('')
            time.sleep(0.8)
            os.system("clear")

"""

    @dispatch(Warrior, Warrior)
    def Attack(self, personaje, enemy):

        personaje.damagePoints(enemy)
        print("El enemigo ahora tiene:", enemy.Life)

    @dispatch(Warrior, Wizard)
    def Attack(self, personaje, enemy):

        personaje.damagePoints(enemy)
        print("El enemigo ahora tiene:", enemy.Life)


    @dispatch(Wizard, Warrior)
    def Attack(self, personaje, enemy):

        personaje.damagePoints(enemy)
        print("El enemigo ahora tiene:", enemy.Life)
"""
