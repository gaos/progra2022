from character import Character
from termcolor import colored
class Warrior(Character):
    """docstring for Warrior."""

    def __init__(self):
        super().__init__()
        self.__status = False


    def toString(self):
        print(colored("[✟]", 'red'))
        print("Es un: Guerrero")
        print("Tiene de salud:", self.Life)

    @property
    def isVulnerable(self):
        return self.__status

    def damagePoints(self, enemy):
        if enemy.isVulnerable:
            enemy.Life = 10
        else:
            enemy.Life = 6
