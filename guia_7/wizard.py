from character import Character
from termcolor import colored
class Wizard(Character):
    """docstring for Wizard."""

    def __init__(self):
        super().__init__()
        self.__status = True
        self.__SpellCast = False


    def toString(self):
        print(colored('☭', 'blue'))
        print("Es un: Mago")
        print("Tiene de salud:", self.Life)
        if self.__SpellCast:
            print("Ha preparado un hechizo")
        else:
            print("No ha preparado un hechizo")
        if self.__status:
            print("Es Vulnerable")
        else:
            print("No es Vulnerable")

    def prepareSpell(self):
        self.__SpellCast = True
        self.__status = False

    def reinicio(self):
        self.__SpellCast = False
        self.__status = True

    def damagePoints(self, enemy):
        if self.__SpellCast and enemy.isVulnerable:
            enemy.Life =  12
        elif self.__SpellCast and enemy.isVulnerable is False:
            enemy.Life = 6
        else:
            enemy.Life = 3

    @property
    def isVulnerable(self):
        return self.__status
    @isVulnerable.setter
    def isVulnerable(self, flag):
        self.__status = flag
