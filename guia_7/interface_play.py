from abc import ABC
from abc import abstractmethod
from abc import ABCMeta
class Interface(ABC):
    """docstring for Interface."""

    def __init__(self):
        pass
    @abstractmethod
    def Choose_enemy(self):
        pass
    @abstractmethod
    def Choose(self):
        pass
    @abstractmethod
    def Attack(self):
        pass
    @abstractmethod
    def animation(self):
        pass
    @abstractmethod
    def hacer_matriz(self):
        pass
    @abstractmethod
    def reiniciar_matriz(self):
        pass
