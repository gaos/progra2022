import random
from alien import Alien
from pistola import Pistola
from te_protones import Cannon
from granate import Granate
from juego import Juego

def hacer_matriz():
    matriz = []
    tamano = int(input('Ingrese el tamaño del escenario:'))
    for i in range(tamano):
        matriz.append([0]*tamano)
    #imprime_matriz(matriz, tamano)
    return matriz

def imprime_matriz(matriz, tamano):
    for i in range(tamano):
        for j in range(tamano):
            print(matriz[i][j], end='')
        print('')

def main():
    """
    cont = 0
    cont = cont + 1
    alien = Alien(2,3, cont)
    print(alien.salud)
    print(alien.is_alive())
    # creacion de armas
    rayo_laser = Pistola()
    cannon = Cannon()
    granate = Granate()

    # prueba de las armas
    rayo_laser.hit_pistola(2,alien)
    cannon.hit_cannon(3,alien)
    granate.hit(1,4, alien)
    print(alien.salud)
    print(alien.is_alive())

    # usar random para el teleport
    rx = random.randint(0,10)
    ry = random.randint(0,10)
    alien.teleport(rx, ry)
    alien.show()

    # contador (temporal) para la cantidad de aliens total
    cont = cont + 1
    other_object = Alien(rx,ry, cont)

    # colision temporal para el metodo de si hay algo
    print(alien.collision_detection(other_object))
    other_object.show()

    # creacion de lista de coordenadas random para creacion random
    lista_coordenadas_r = []
    for i in range(10):
        r0 = random.randint(0,10)
        r1 = random.randint(0,10)
        lista_temp = [r0, r1]
        lista_coordenadas_r.append(lista_temp)
    print(lista_coordenadas_r)
    coleccion_aliens = []
    # creacion de aliens aleatoria
    for i in range(len(lista_coordenadas_r)):
        cont = cont + 1
        alien_new = Alien(lista_coordenadas_r[i][0], lista_coordenadas_r[i][1],
                           cont)
        coleccion_aliens.append(alien_new)
    for alien in coleccion_aliens:
        alien.show()
    # es para comprobar las funciones especificas, los puntos mediante rangos
    """
    matriz = hacer_matriz()
    #print(len(matriz))
    # imprime_matriz(matriz, len(matriz))
    juego = Juego(matriz)
    juego.matriz
    juego.creacion_aliens()
    print("Cambio")
    juego.matriz
    juego.menu_armas()
    juego.matriz
    # funcion para el teleport y cambio, desactivada
    #juego.aliens_cambio()
    #juego.matriz
if __name__ == '__main__':
    main()
