from alien_0 import Alien_0

class Alien(Alien_0):
    """docstring for Alien."""

    def __init__(self, x, y, cont):
        super().__init__()
        self.__salud = 3
        self.__c_x = x
        self.__c_y = y
        self.alien_contador = cont
    def collision_detection(self, objeto):
        return False
    @property
    def salud(self):
        return self.__salud
    @salud.setter
    def salud(self, vida):
        self.__salud = vida

    def teleport(self, x,y):
        self.__c_x = x
        self.__c_y = y

    def hit(self):
        # siempre habrá un hit si es mayor a 0
        if self.__salud > 0:
            self.__salud = self.__salud - 1
            print("Ha dado en el blanco")
    def is_alive(self):
        if self.__salud > 0:
            return True
        else:
            return False
    def show(self):
        print("El alien esta:", self.__c_x, ",", self.__c_y)
        print("Tiene de vida:", self.__salud)
        print("Aliens creados:", self.alien_contador)
    @property
    def c_x(self):
        return self.__c_x
    @c_x.setter
    def c_x(self, random):
        self.__c_x = random
    @property
    def c_y(self):
        return self.__c_y
    @c_x.setter
    def c_y(self, random):
        self.__c_y = random
