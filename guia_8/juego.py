import random
from alien import Alien
from pistola import Pistola
from te_protones import Cannon
from granate import Granate

class Juego():
    """docstring for Juego."""

    def __init__(self, matriz):
        self.__matriz = matriz
        self.__cont = 0
        self.__lista_aliens = []


    @property
    def matriz(self):
        alien_temp = Alien(0, 0, 0)
        for i in range(len(self.__matriz)):
            for j in range(len(self.__matriz)):
                if (type(self.__matriz[i][j]) == type(alien_temp) and
                    self.__matriz[i][j].is_alive()):
                    print("1", end='')
                else:
                    print("0", end='')
            print('')
        print("1 es un alien vivo, 1 es un alien malo")
    def creacion_aliens(self):
        # fijado en 2 por ahora
        print("Entro")
        cantidad = 2
        for i in range(cantidad):
            r0 = random.randint(0,len(self.__matriz)-1)
            r1 = random.randint(0,len(self.__matriz)-1)
            self.__cont = self.__cont + 1
            alien_new = Alien(r0,r1,self.__cont)
            self.__matriz[alien_new.c_x][alien_new.c_y] = alien_new
            self.__lista_aliens.append(alien_new)


    def menu_armas(self):
        print("¿Qué armas desea usar?")
        print("1. Pistola")
        print("2. Cañon")
        print("3. Granada")
        opc = int(input(""))
        if opc == 1:
            print("Se está usando la pistola")
            print("La pistola es un rayo de luz en una linea")
            print("Esta linea es recta en x")
            x = int(input("Ingrese desde donde lanza su ataque:"))
            pistola = Pistola()
            for alien in self.__lista_aliens:
                pistola.hit_pistola(x, alien)
        elif opc == 2:
            print("Se está usando el cañon")
            print("El cañon te protones son 3 rayos en una linea c/u")
            print("Estas 3 lineas son rectas en x")
            x = int(input("Ingrese desde donde lanza su ataque:"))
            cannon = Cannon()
            for alien in self.__lista_aliens:
                cannon.hit_cannon(x, alien)
        elif opc == 3:
            print("Se está usando la Granada")
            print("La granada es un cuadrado 3x3 en el area y hace mas daño")
            print("Para esto se necesita x y tambien y")
            x = int(input("Ingrese x de donde lanza su ataque:"))
            y = int(input("Ingrese y de donde lanza su ataque:"))
            granate = Granate()
            for alien in self.__lista_aliens:
                granate.hit(x,y, alien)

    # intento de teleport y colision
    """
    def aliens_cambio(self):
        r0 = random.randint(0,len(self.__matriz)-1)
        r1 = random.randint(0,len(self.__matriz)-1)
        alien_temp = Alien(0, 0, 0)
        for alien in self.__lista_aliens:
            for i in range(len(self.__matriz)):
                for j in range(len(self.__matriz)):
                    if (type(self.__matriz[i][j]) == type(alien_temp) and
                        self.__matriz[i][j].is_alive()):
                        if(alien.c_x == i and alien.c_y == j):
                            pass
                        else:
                            alien.hit()
                            alien.hit()
                            alien.hit()
                            self.__matriz[i][j].hit()
                            self.__matriz[i][j].hit()
                            self.__matriz[i][j].hit()
                            print("Mueren dos aliens por teleport")
                    else:
                        self.__matriz[r0][r1] = alien
                        self.__matriz[i][j] = 0
                        print()
        """
