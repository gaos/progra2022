class Plant(object):
    """docstring for Plant."""

    def __init__(self):
        self.__propietario = ""
        self.__solcito = 0

    @property
    def propietario(self):
        return self.__propietario
    @propietario.setter
    def propietario(self, nombre):
        self.__propietario = nombre
    @property
    def sol(self):
        return self.__solcito
    @sol.setter
    def sol(self, value):
        self.__solcito = value
