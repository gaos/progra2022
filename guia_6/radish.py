from plantitas import Plant
class Rabano(Plant):
    """docstring for Rabano ( ͡° ͜ʖ ͡°)."""

    def __init__(self):
        super().__init__()
        self.__planta = "Rabano"
        # 20-30 dias soleado, 30-70 dias nublado, 25-40 con lluvia
        # se ocupara 1/ en cantidad, cuando alcance 1 o supere esta listo
        self.__dias = 0

    @property
    def planta(self):
        return self.__planta
    @property
    def maduracion(self):
        return self.__dias
    @maduracion.setter
    def maduracion(self, condicion):
        if condicion == "con Lluvia":
            print("lluvia")
            r1 = random.randint(25, 40)
            self.__dias = self.__dias + 1/r1
        elif condicion == "Soleado":
            r1 = random.randint(20, 30)
            self.__dias = self.__dias + 1/r1
            print("soleado")
        elif condicion == "Nublado":
            print("nublado")
            r1 = random.randint(30, 70)
            self.__dias = self.__dias + 1/r1
        else:
            print("no crece")
