from plantitas import Plant
class Violeta(Plant):
    """docstring for Violeta."""

    def __init__(self):
        super().__init__()
        self.__planta = "Violeta"
        # necesita humedad y nublado para crecer, ademas de sol, 180 dias
        self.__dias = 0

    @property
    def planta(self):
        return self.__planta
    @property
    def maduracion(self):
        return self.__dias
    @maduracion.setter
    def maduracion(self, condicion):
        if condicion == "con Lluvia":
            print("")
        elif condicion == "Soleado":
            print("")
        elif condicion == "Nublado":
            print("")
        else:
            print("No crece sin luz")
