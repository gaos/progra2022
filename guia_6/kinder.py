import random
from plantitas import Plant
from weed import Hierba
from viola  import Violeta
from radish import Rabano
from black_clover import Trebol

class Garden():
    """La clase garden es el jardín donde estan las acciones"""

    def __init__(self, objeto):
        if objeto.sol == 1:
            estado = ["con Lluvia", "Soleado", "Nublado"]
            self.__estado = random.choice(estado)
        else:
            self.__estado = "Lejos de la ventana"
    @property
    def estado(self):
        print("Actualmente está: ", self.__estado)
        return self.__estado
    @estado.setter
    def estado(self, estado):
        self.__estado = estado
