import random
from kinder import Garden
from plantitas import Plant
from weed import Hierba
from viola  import Violeta
from radish import Rabano
from black_clover import Trebol
"""
    Son doce niños, cada uno tiene 4 vasos con 4 posibles plantas,
    las plantas son: rabano, violeta, trebol,
    treboles: https://www.youtube.com/watch?v=2a0LK2RJiQk
    hierba: https://www.youtube.com/watch?v=8t3vD_3shwI
    hierba (funable):https://www.youtube.com/results?search_query=weed+growing+time+lapse
    rabano: https://www.youtube.com/watch?v=4EbpN0ghH9A
    violeta: https://www.youtube.com/watch?v=ZuF27b-nfnk
    ¿como que la violeta no es violeta?
"""

def crecimiento(objeto):
    if objeto.sol == 0:
        print("El sol no esta con los pecadores")
    elif objeto.sol == 1:
        print("Praise the sun")
    else:
        print("no sirve")

def trabajando_jardin(lista_peques):
    lista_al_sol = []
    lista_sin_sol = []
    for i in range(len(lista_peques)):
        for j in range(4):
            # para 4 plantas por niños
            random_planta = random.randint(0,3)
            if random_planta == 0:
                # hierba
                planta = Hierba()
            elif random_planta == 1:
                # violeta
                planta = Violeta()
            elif random_planta == 2:
                # treboles
                planta = Trebol()
            elif random_planta == 3:
                # Rabano
                planta = Rabano()
            planta.propietario = lista_peques[i]
            if j == 1 or j == 2:
                planta.sol = 1
                lista_al_sol.append(planta)
            else:
                planta.sol = 0
                lista_sin_sol.append(planta)


    # estado y planta
    for i in range(len(lista_al_sol)):
        jardin = Garden(lista_al_sol[i])
        clima = jardin.estado
        print(clima)
        print(lista_al_sol[i].planta)
        print(lista_al_sol[i].propietario)
    for i in range(len(lista_sin_sol)):
        jardin = Garden(lista_sin_sol[i])
        clima = jardin.estado
        print(clima)
        print(lista_sin_sol[i].planta)
        print(lista_sin_sol[i].propietario)
    print("[ventana][ventana][ventana]")
    for i in range(len(lista_al_sol)):
        if lista_al_sol[i].planta == "Trebol":
            print("T",end="")
        elif lista_al_sol[i].planta == "Violeta":
            print("V",end="")
        elif lista_al_sol[i].planta == "Hierba":
            print("H",end="")
        elif lista_al_sol[i].planta == "Rabano":
            print("R",end="")
    print("")
    for i in range(len(lista_sin_sol)):
        if lista_sin_sol[i].planta == "Trebol":
            print("T",end="")
        elif lista_sin_sol[i].planta == "Violeta":
            print("V",end="")
        elif lista_sin_sol[i].planta == "Hierba":
            print("H",end="")
        elif lista_sin_sol[i].planta == "Rabano":
            print("R",end="")
    print("")

def main():
    lista_peques = ["Alicia", "Marit", "Pepito", "David", "Eva", "Lucia",
                    "Rocío", "Andrés", "José", "Belen", "Sergio", "Larry"]

    trabajando_jardin(lista_peques)
    """
    print("Hola, meme")
    planta = Plant()
    planta.propietario = "Salitre"
    print(planta.propietario)
    print(planta.sol)
    hierba = Hierba()
    hierba.propietario = "Tengo hierba"
    print(hierba.propietario)
    mi = Rabano()
    crecimiento(mi)
    jardin = Garden(mi)
    jardin.estado
    mi.sol = 1
    crecimiento(mi)
    # de esta forma se entra a ver el precio del sol
    jardin = Garden(mi)
    jardin.estado
    """
if __name__ == '__main__':
    main()
