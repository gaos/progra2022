from plantitas import Plant
class Trebol(Plant):
    """docstring for Trebol."""

    def __init__(self):
        super().__init__()
        self.__planta = "Trebol"
        # 2-3 dias soleado, una semana sin luz
        self.__dias = 0

    @property
    def planta(self):
        return self.__planta
    @property
    def maduracion(self):
        return self.__dias
    @maduracion.setter
    def maduracion(self, condicion):
        if condicion == "con Lluvia":
            print("")
        elif condicion == "Soleado":
            print("")
            r1 = random.randint(2, 3)
            self.__dias = self.__dias + 1/r1
        elif condicion == "Nublado":
            print("")
        else:
            print("La gracia de Cristo")
