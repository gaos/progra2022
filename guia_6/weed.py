from plantitas import Plant
class Hierba(Plant):
    """docstring for Hierba."""

    def __init__(self):
        super().__init__()
        self.__planta = "Hierba"
        # 7-14 semanas, condiciones nublado, soleado, no lluvia
        self.__dias = 0

    @property
    def planta(self):
        return self.__planta
    @property
    def maduracion(self):
        return self.__dias
    @maduracion.setter
    def maduracion(self, condicion):
        if condicion == "con Lluvia":
            print("No crece")
        elif condicion == "Soleado":
            print("")
        elif condicion == "Nublado":
            print("")
        else:
            # no crece a menos que tenga cuidados que no harán niños
            print("No crece")
