# ejercicio 4

class Astro():
    def __init__(self, masa, densidad, diametro, dist_sol, id, apodo):
        # Constructor del planeta con todos sus atributos
        self.masa = masa
        self.densidad = densidad
        self.diametro = diametro
        self.dist_sol = dist_sol
        self.id = id
        self.apodo = apodo

    def reporte_planeta(self):
        # Se muestra toda la info de un planeta en un reporte
        print(f"PLANETA {self.id}, '{self.apodo}'")
        print(f"densidad: {self.densidad} kg/m³")
        print(f"diametro: {self.diametro} km")
        print(f"distancia al sol: {self.dist_sol} km")
        print(f"masa: {self.masa} kg\n")

if __name__ == "__main__":
    # Primer planeta, con sus caracteristicas y reporte
    planeta1 = Astro(1500000, 20500, 30000, 150000, 15,"francisco")
    planeta1.reporte_planeta()

    # Segundo planeta, con sus caracteristicas y reporte
    planeta2 = Astro(100000, 26700, 15780, 40000, 99, "Hormazabal")
    planeta2.reporte_planeta()
