#Ejercicio 3

class Protein():
	def __init__(self, org, mut, system_e, f_ing, f_rld, uid, apodo,
					pdb_oi, autor, organizacion, clasif):
		#Constructor con todos los atributos de la molecula
		self.uid = uid
		self.apodo = apodo
		self.pdb_oi = pdb_oi
		self.clasif = clasif
		self.org = org
		self.system_e = system_e
		self.mut = mut
		self.f_ing = f_ing
		self.f_rld = f_rld
		self.autor = autor
		self.organizacion = organizacion
	
	def reporte (self):
		#Se hace un reporte con toda la informacion
		print("Reporte")
		print(f"Molecula {self.uid}, '{self.apodo}'")
		print(f"PDB DOI: {self.pdb_oi}")
		print(f"Clasificacion: {self.clasif}")
		print(f"organismo: {self.org}")
		print(f"sistema de expresion: {self.system_e}")
		print(f"mutaciones: {self.mut}")
		print(f"fecha de ingreso: {self.f_ing}")
		print(f"fecha liberada: {self.f_rld}")
		print(f"Autor: {self.autor}")
		print(f"Organizacion/es: {self.organizacion}")
		
if __name__ == "__main__":
	#Se crea una proteina de ejemplo y se muestra su informacion
	proteina1 = Protein("protozoo", "Si", "protozoos", "20-06-2094",
	"21-07-2094", "15U7H", "Molecula grande", "23453.cv/sjghl.com",
	"Sebastian", "Utal", "MICROORGANISMO")
	proteina1.reporte()
		
		
		
	
			
	
