from diploma import certificado

class Asignatura():
    def __init__(self):
        self.__estudiante = []
        self.__nombre = ""

    @property
    def estudiante(self):
        return self.__estudiante
    @estudiante.setter
    def estudiante(self, estu):
        self.__estudiante.append(estu)

    @property
    def nombre(self):
        return self.__nombre
    @nombre.setter
    def nombre(self, name):
        self.__nombre = name
