import random
from persona import estudiante
from ramo import Asignatura
from diploma import certificado

"""
Se generan la lista y certificado, se le asigna al estudiante el certificado
Luego se remueve de la lista, esta lista varia el valor que se le deja al
estudiante, asi cada uno tiene un certificado al azar, en ocasiones tienen el
certificado correspondiente por lo que se hace una comparación y se hace una
lista en la cual no coinciden el certificado, lista que se debe buscar entre
estudiantes.
"""
def creacion_nombre(objeto):
    """
    Debido a que en el momento de ejecucion se asigna en memoria utilizando
    el rut (la asignacion a memoria del objeto), se creara un nombre distinto
    mediante los ultimos 5 numeros de la memoria, el ultimo numero indicara el
    genero, los siguientes dos el nombre, los ultimos 2 los apellidos
    """
    # Aqui va la lista para colocar el nombre
    nombre = []
    # Aqui se pudo ahorrar variables cambiando de lado la lista, pero las deje
    # aqui porque de esta forma la variable eran mas faciles de encontrar
    primer_nombre_f = ["Lucía","Sofía","Martina","Regina","Julia","Paula",
                       "Carla","Juana","Luna","Ariadna"]
    segundo_nombre_f = ["Andrea","Emma","Alba","Noa","Ana","Laia","Ona","Mar",
                        "Ada","Mara"]
    primer_nombre_m = ["Mateo","Martín","Alejandro","Diego","Adrián","Álvaro",
                       "Pablo","Lucas","Daniel","Denis"]
    segundo_nombre_m = ["Reyes","Charlie","Tyler","Nain","Giuseppe","Napoleón",
                        "Adolf","Jacobo","Astor","Adal"]
    primer_apellido = ["Benítez","Castillo","Castro","Contreras","De León",
                       "Díaz","Duarte","Espinoza","Fernández","Flores"]
    segundo_apellido = ["García","Giménez","Gómez","Gonzáles","Gutiérrez",
                        "Hernández","López","Mamani","Martínez","Hitler"]
    # se transforma la lista en str para separarla
    lista = list(str(objeto.get_rut()))

    for i in range(len(lista)):

        if i == 0 and int(lista[len(lista)-i-1]) > 4:
            objeto.set_genero(True)
        elif i == 0:
            objeto.set_genero(False)
        if i == 1 and objeto.get_genero():
            nombre.append(primer_nombre_m[int(lista[len(lista)-i-1])])
        elif i == 1 and objeto.get_genero() == False:
            nombre.append(primer_nombre_f[int(lista[len(lista)-i-1])])
        if i == 2 and objeto.get_genero():
            nombre.append(segundo_nombre_m[int(lista[len(lista)-i-1])])
        elif i == 2 and objeto.get_genero() == False:
            nombre.append(segundo_nombre_f[int(lista[len(lista)-i-1])])
        if i == 3:
            nombre.append(primer_apellido[int(lista[len(lista)-i-1])])
        if i == 4:
            nombre.append(segundo_apellido[int(lista[len(lista)-i-1])])

    objeto.set_nombre(nombre)

def menu():

    i = 0
    lista_est = []
    lista_asignacion = []
    ramos_carrera = []
    lista_diplomas = []
    lista_no = []

    print("Bienvenido a la fila premiacion mejor del ramo")
    print("Abreviado FPMR, en el FPMR simulara una cantidad de personas")
    print("Las cuales recibiran un diploma que el que llega lo agarra")
    print("Luego cambia el diploma con el correspondiente propio, suerte\n")

    cantidad = int(input("Ingrese la cantidad de ramos (1/3 de certificados):"))
    for i in range((cantidad*3)):
        # creacion y guardado en lista de personas
        persona = estudiante()
        persona.set_rut(id(persona))
        lista_est.append(persona)
        lista_asignacion.append(persona)
        creacion_nombre(persona)

    #nombres_r = ["algebra", "fisica", "progra"]
    for i in range(cantidad):
        #lista de asignaturas
        ramo = Asignatura()
        ramo.nombre = id(ramo)
        ramos_carrera.append(ramo)

    for i in range((cantidad*3)):
        diploma = certificado(lista_est[i].get_rut(),lista_est[i].get_nombre())
        lista_diplomas.append(diploma)

    print("La cantidad de la lista personas es:",len(lista_est))
    print("La cantidad de la lista ramos es:",len(ramos_carrera))
    print("La cantidad de la lista diplomas es:",len(lista_diplomas))


    for i in range(len(lista_est)):

        r0 = random.randint(0,len(lista_diplomas)-1)
        #print(r0)
        lista_est[i].set_diploma(lista_diplomas[r0])
        print("Al estudiante:",lista_est[i].get_nombre(), "tiene el diploma de:",
              lista_diplomas[r0].get_nombre())

        lista_diplomas.remove(lista_diplomas[r0])

    for i in range(len(lista_est)):

        if lista_est[i].get_diploma().get_rut() == lista_est[i].get_rut():
            print("El diploma corresponde en el estudiante N°: ", i+1)
        else:
            lista_no.append(lista_est[i])

    print("Ahora intercambiando entre dos estudiantes sucesivamente:")

    # intento buscador, es con rut en vez de nombre por si comparten el nombre
    for i in range(len(lista_no)):

        for j in range(len(lista_no)):

            if (lista_no[i].get_rut() == lista_no[j].get_diploma().get_rut() and
                 i is not j):

                print("El estudiante:", lista_no[i].get_nombre(), "intercambia",
                      "con el estudiante: ", lista_no[j].get_nombre())

                diploma_en_mesa = lista_no[i].get_diploma()
                lista_no[i].set_diploma(lista_no[j].get_diploma())
                lista_no[j].set_diploma(diploma_en_mesa)

    for i in range(len(lista_no)):

        if lista_no[i].get_diploma().get_rut() == lista_no[i].get_rut():

            print("El diploma ", i+1, " corresponde al estudiante: ",
                  lista_est[i].get_nombre())


def main():
    menu()

if __name__ == '__main__':
    main()
