
class estudiante():
    def __init__(self):
        # 1 o muchos diplomas
        self.diploma = None
        # 1 o mas asignaturas
        self.asignatura = []
        # rut para asignar el nombre
        self.rut = None
        self.genero = None
        self.nombre = None

    def set_genero(self, flag):
        self.genero = flag

    def get_genero(self):
        return self.genero

    def set_nombre(self, nombre):
        self.nombre = nombre
        
    def get_nombre(self):
        return self.nombre

    def set_asignatura(self, ramo):
        self.asignatura.append(ramo)

    def get_asignatura(self):
        return self.asignatura

    def get_diploma(self):
        return self.diploma

    def set_diploma(self, diploma):
        self.diploma = diploma

    def set_rut(self, rut):
        self.rut = rut
    def get_rut(self):
        return self.rut
