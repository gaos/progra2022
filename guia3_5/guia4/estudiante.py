from asignatura import Asignatura

class Estudiante(object):
    """docstring for estudiante."""

    def __init__(self, nombre):
        self._nombre = nombre
        self._asignatura = []
        pass
    @property
    def nombre(self):
        return self._nombre
    @nombre.setter
    def nombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")
    @property
    def asignatura(self):
        return self._asignatura
    @asignatura.setter
    def asignatura(self, asignatura):
        if isinstance(asignatura, Asignatura):
            self._asignatura.append(asignatura)
        else:
            print("El tipo de dato no corresponde")
