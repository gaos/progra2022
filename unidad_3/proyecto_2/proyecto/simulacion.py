import random as ran
from vacuna import Vacunas
import json

class Simulacion():
    """docstring for Simulacion."""

    def __init__(self, comunidad, enfermedad):
        self.__comunidad = comunidad
        self.__enfermedad = enfermedad
        self.__array = []
        self.__auxiliar = []
        self.__vacunas_total = []
        valor = int(len(self.__comunidad.lista_ciudadanos)/2)
        # las dosis deben de ser 9-6-3 tipo 1-2-3
        # 9+6+3=18, 9/18 = 0.5 6/15 = 0.25 3/15 = 0.125
        # i valor/2 = 9/18, i<=(valor*(9/18)) and i> (valor*(3/18)) == 9*6
        # i< (valor*(3/18)) == 0.125
        for i in range(valor):
            if(i > (valor/2)):
                vacuna = Vacunas(1,1)
                self.__vacunas_total.append(vacuna)

            elif(i<=(valor*(9/18)) and i> (valor*(3/18))):
                vacuna = Vacunas(2,2)
                self.__vacunas_total.append(vacuna)

            elif(i< (valor*(3/18))):
                vacuna = Vacunas(3,2)
                self.__vacunas_total.append(vacuna)


    def vacunacion(self, dia):
        vacuna_al_dia = int(len(self.__comunidad.lista_ciudadanos)/20)
        ran.shuffle(self.__vacunas_total)
        # lista para saber que vacunas fueron las eliminadas
        lista_temporal = []
        # se vacunan solo si hay vacunas disponibles
        for i in range(vacuna_al_dia):
            azaroso = ran.randint(0,(len(self.__comunidad.lista_ciudadanos) -1))
            if (self.__comunidad.lista_ciudadanos[azaroso].estado and
                self.__comunidad.lista_ciudadanos[azaroso].sano and
                self.__comunidad.lista_ciudadanos[azaroso].inmune == False and
                len(self.__vacunas_total) !=0):
                self.__comunidad.lista_ciudadanos[azaroso].vacuna = self.__vacunas_total[len(self.__vacunas_total)-1]
                self.__vacunas_total.pop(len(self.__vacunas_total)-1)
                if self.__comunidad.lista_ciudadanos[azaroso].vacuna.cantidad == 1:
                    self.__comunidad.lista_ciudadanos[azaroso].vacuna.paso_1 = dia
                    self.__comunidad.lista_ciudadanos[azaroso].vacuna.paso_2 = dia
                else:
                    self.__comunidad.lista_ciudadanos[azaroso].vacuna.paso_1 = dia
                    self.__comunidad.lista_ciudadanos[azaroso].vacuna.paso_2 = dia + 6


    def run(self, numero_dias):
        for self.dia in range(1,numero_dias):
            self.contagio_x_contacto(self.dia)
            self.imprimir_contagiados()
            self.crear_diccionario()
            if self.dia >= 3 and len(self.__vacunas_total) != 0:
                self.vacunacion(self.dia)
        self.write_data()

    def contagio_x_contacto(self, numero_dias):
        contador = 0

        for i in range(len(self.__comunidad.lista_ciudadanos)):

            # ya fue añadido grave como sub estado, se podria hacer en estado, pero preferi un boolean
            potencial_contagio = False
            for k in range(len(self.__comunidad.lista_ciudadanos[i].familia)):
                if (self.__comunidad.lista_ciudadanos[self.__comunidad.\
                    lista_ciudadanos[i].familia[k]].estado and
                    self.__comunidad.lista_ciudadanos[self.__comunidad.\
                    lista_ciudadanos[i].familia[k]].sano ==False and
                    self.__comunidad.lista_ciudadanos[self.__comunidad.\
                    lista_ciudadanos[i].familia[k]].grave == False):
                    potencial_contagio = True

            #if para que se infecten si tienen un familiar
            if(self.__comunidad.lista_ciudadanos[i].estado and
               self.__comunidad.lista_ciudadanos[i].inmune==False and
               self.__comunidad.lista_ciudadanos[i].sano and
               potencial_contagio):
                # para enfermar segun probabilidad y la cantidad de personas que se asocian
                random = ran.randint(0,100)
                #probabilidad conexion es para los contactos estrechos
                if(self.__comunidad.lista_ciudadanos[i].vacuna.tipo != 1 and
                    self.__comunidad.lista_ciudadanos[i].vacuna.paso_2 < numero_dias):
                    if (random < (self.__enfermedad.probabilidadInfeccion *\
                         self.__comunidad.probabilidad_conexion*100)):
                        self.__auxiliar.append(i)
                        if(self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 2 or
                           self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 3):
                            self.__comunidad.lista_ciudadanos[i].vacuna = Vacunas(0,0)
                elif self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 1:
                    if (random < (self.__enfermedad.probabilidadInfeccion *\
                         self.__comunidad.probabilidad_conexion*100*0.75)):
                        self.__auxiliar.append(i)
                elif (self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 2 and
                      self.__comunidad.lista_ciudadanos[i].vacuna.paso_2 > \
                      numero_dias):
                    if (random < (self.__enfermedad.probabilidadInfeccion *\
                         self.__comunidad.probabilidad_conexion*100)):
                        self.__auxiliar.append(i)
                elif (self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 3 and
                      self.__comunidad.lista_ciudadanos[i].vacuna.paso_2 > \
                      numero_dias):
                    self.__comunidad.lista_ciudadanos[i].inmune=True

            # para encuentros segun personas al azar en la calle
            for k in range(self.__comunidad.gente):
                persona_azar = ran.randint(0,(len(self.__comunidad.lista_ciudadanos)-1))
                if(self.__comunidad.lista_ciudadanos[i].estado and
                   self.__comunidad.lista_ciudadanos[i].inmune==False and
                   self.__comunidad.lista_ciudadanos[i].sano and
                   self.__comunidad.lista_ciudadanos[persona_azar].sano== False
                   and self.__comunidad.lista_ciudadanos[persona_azar].estado
                   and self.__comunidad.lista_ciudadanos[persona_azar].grave==\
                   False):
                    random = ran.randint(0,100)
                    #probabilidad conexion es para los contactos estrechos
                    if (self.__comunidad.lista_ciudadanos[i].vacuna.tipo != 1 and
                         self.__comunidad.lista_ciudadanos[i].vacuna.paso_2 < numero_dias):
                        if (random < (self.__enfermedad.probabilidadInfeccion *\
                             self.__comunidad.probabilidad_conexion*100)):
                            self.__auxiliar.append(i)
                            # pierde la vacuna si se enferma y tenia 1 dosis
                            if(self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 2 or
                               self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 3):
                                self.__comunidad.lista_ciudadanos[i].vacuna = Vacunas(0,0)
                    elif self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 1:
                        if (random < (self.__enfermedad.probabilidadInfeccion *\
                             self.__comunidad.probabilidad_conexion*100*0.75)):
                            self.__auxiliar.append(i)
                    elif (self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 2 and
                          self.__comunidad.lista_ciudadanos[i].vacuna.paso_2 > \
                          numero_dias):
                        if (random < (self.__enfermedad.probabilidadInfeccion *\
                             self.__comunidad.probabilidad_conexion*100)):
                            self.__auxiliar.append(i)
                    elif (self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 3 and
                          self.__comunidad.lista_ciudadanos[i].vacuna.paso_2 > \
                          numero_dias):
                        self.__comunidad.lista_ciudadanos[i].inmune=True


            """
            fragmento que determina si caso activo sana
            tambien reduce la cantidad de la mortalidad para los no graves
            """
            if(self.__comunidad.lista_ciudadanos[i].estado and
               self.__comunidad.lista_ciudadanos[i].sano==False and
               (numero_dias) == self.__enfermedad.promedioPasos+\
                   self.__comunidad.lista_ciudadanos[i].contador):
                probabilidad_muerte = ran.randint(0,100)

                if (self.__comunidad.lista_ciudadanos[i].grave and
                    self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 0):
                    if probabilidad_muerte <= self.__enfermedad.mortalidad:
                        self.__comunidad.lista_ciudadanos[i].sano = False
                        self.__comunidad.lista_ciudadanos[i].estado = False
                        self.__comunidad.lista_ciudadanos[i].infectado = False
                    else:
                        self.__comunidad.lista_ciudadanos[i].sano = True
                        self.__comunidad.lista_ciudadanos[i].infectado = False
                elif(self.__comunidad.lista_ciudadanos[i].grave and
                    self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 1):
                    if probabilidad_muerte <= self.__enfermedad.mortalidad*0.75:
                        self.__comunidad.lista_ciudadanos[i].sano = False
                        self.__comunidad.lista_ciudadanos[i].estado = False
                        self.__comunidad.lista_ciudadanos[i].infectado = False
                    else:
                        self.__comunidad.lista_ciudadanos[i].sano = True
                        self.__comunidad.lista_ciudadanos[i].infectado = False
                else:
                    """
                     Es solo para los vacuna tipo 0 porque los tipo 1 tienen
                     otro porcentaje y los tipos 2 no es grave
                    este es para los que no estan graves, su mortalidad es menor
                    """
                    if (probabilidad_muerte <= (self.__enfermedad.mortalidad*0.6) and
                        self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 0):
                        self.__comunidad.lista_ciudadanos[i].sano = False
                        self.__comunidad.lista_ciudadanos[i].estado = False
                        self.__comunidad.lista_ciudadanos[i].infectado = False
                    # vacuna tipo 1
                    elif(probabilidad_muerte <= (self.__enfermedad.mortalidad*0.6*0.75) and
                        self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 1):
                        self.__comunidad.lista_ciudadanos[i].sano = False
                        self.__comunidad.lista_ciudadanos[i].estado = False
                        self.__comunidad.lista_ciudadanos[i].infectado = False
                    else:
                        self.__comunidad.lista_ciudadanos[i].sano = True
                        self.__comunidad.lista_ciudadanos[i].infectado = False
        """
        aqui no estan el tipo 2 o tipo 3, porque si fueran tipo 2-3 que no estan
        a tiempo las vuelve tipo 0 (la vacuna tipo 0,0 son los que no tienen
        vacunas) y las que estan a tiempo nunca se vuelven grave
        """
        # aqui se hace una lista auxiliar la cual menciona a cuales infectar
        for i in range(len(self.__auxiliar)):
            self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].sano = False
            self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].inmune = True
            self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].infectado = True
            self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].contador = numero_dias
            # los que tienen alguna afeccion o enfermedad base provoca que sea grave
            if(self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].afeccion != "No tiene" and
               self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].base != "No tiene" and
               (self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].vacuna.tipo == 0 or
                self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].vacuna.tipo == 1)):
                posibilidad_de_grave = ran.randint(1,4)
                if self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].vacuna.tipo == 0:
                    self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].grave = True
                elif (self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].vacuna.tipo == 1 and
                      posibilidad_de_grave > 1):
                    self.__comunidad.lista_ciudadanos[self.__auxiliar[i]].grave = True



        # reinicia la lista dejandola vacia
        for i in range(len(self.__auxiliar)):
            self.__auxiliar.pop(len(self.__auxiliar)-1)

        # se encarga de ver si se vuelven graves los que no tienen afecciones
        for i in range(len(self.__comunidad.lista_ciudadanos)):
            if (self.__comunidad.lista_ciudadanos[i].sano == False and
                self.__comunidad.lista_ciudadanos[i].infectado and
                self.__comunidad.lista_ciudadanos[i].estado):
                posibilidad_de_grave = ran.randint(0,100)
                if (posibilidad_de_grave <= self.__enfermedad.gravedad and
                    self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 0):
                    self.__comunidad.lista_ciudadanos[i].grave = True
                if(posibilidad_de_grave <= (self.__enfermedad.gravedad*0.75) and
                    self.__comunidad.lista_ciudadanos[i].vacuna.tipo == 1):
                    self.__comunidad.lista_ciudadanos[i].grave = True

    def crea_contactos(self):

        avance = 0
        while avance < len(self.__comunidad.lista_ciudadanos):
            for i in range(self.__comunidad.promedio_conexion):
                random = ran.randint(0,(len(self.__comunidad.lista_ciudadanos)-1))
                # si listas no estan llenas sobre el promedio de conexion
                if (len(self.__comunidad.lista_ciudadanos[avance].familia) < \
                    self.__comunidad.promedio_conexion and
                    len(self.__comunidad.lista_ciudadanos[random].familia)< \
                        self.__comunidad.promedio_conexion):
                    # añade a lista si no se tiene familia
                    self.__comunidad.lista_ciudadanos[avance].familiaAdd(random)
            avance = avance + 1

    def imprimir_contagiados(self):

        self.__casos_activos = 0
        self.__poblacion = 0
        self.__poblacion_susceptible = 0
        self.__poblacion_inmune = 0

        for i in range(len(self.__comunidad.lista_ciudadanos)):
            if (self.__comunidad.lista_ciudadanos[i].estado):
                if(self.__comunidad.lista_ciudadanos[i].sano == True and
                self.__comunidad.lista_ciudadanos[i].inmune == False):
                    self.__poblacion_susceptible = self.__poblacion_susceptible + 1
                if(self.__comunidad.lista_ciudadanos[i].inmune == True):
                    self.__poblacion_inmune = self.__poblacion_inmune + 1
                if(self.__comunidad.lista_ciudadanos[i].infectado == True):
                    self.__casos_activos = self.__casos_activos + 1
                self.__poblacion = self.__poblacion + 1

        self.__poblacion_sanada = self.__poblacion_inmune - self.__casos_activos

        print("--------------------------------------------------")
        #print("Nuevos contagios: ", casos_nuevos_contagios)
        print("Dia:", self.dia)
        print("Vacunas disponibles:", len(self.__vacunas_total))
        print("\nPoblacion total (vivos): ", self.__poblacion)
        print("Casos activos:", self.__casos_activos)
        print("Susceptibles a enfermarse: ", self.__poblacion_susceptible)
        print("Sanados de enfermedad (vivos): ", self.__poblacion_sanada)
        print("Fallecidos: ", len(self.__comunidad.lista_ciudadanos)-self.__poblacion)

        print("--------------------------------------------------")

    """ se crea diccionario """
    def crear_diccionario(self):

        self.__dict ={
            "Dia: " : self.dia,
            "Poblacion total (vivos)" : self.__poblacion,
            "Casos activos:" : self.__casos_activos,
            "Susceptibles a enfermarse: " : self.__poblacion_susceptible,
            "Sanados de enfermedad (vivos): " : self.__poblacion_sanada,
            "Fallecidos: ": len(self.__comunidad.lista_ciudadanos)-self.__poblacion
        }
        self.__array.append(self.__dict)


    def write_data(self):

        with open("data.json", "w") as file:
            json.dump(self.__array, file, indent=4)
