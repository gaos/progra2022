class Enfermedad():
    """docstring for Enfermedad."""

    def __init__(self, probabilidadInfeccion, promedio_pasos, mortal, gravedad):
        self.__probabilidadInfeccion =  probabilidadInfeccion
        self.__promedio_pasos = promedio_pasos
        self.__mortalidad = mortal
        self.__gravedad = gravedad

    #getters and setters
    @property
    def probabilidadInfeccion(self):
        return self.__probabilidadInfeccion

    @probabilidadInfeccion.setter
    def probabilidadInfeccion(self, variable):
        self.__probabilidadInfeccion = variable

    @property
    def gravedad(self):
        return self.__gravedad

    @gravedad.setter
    def gravedad(self, variable):
        self.__gravedad = variable

    @property
    def promedioPasos(self):
        return self.__promedio_pasos

    @promedioPasos.setter
    def promedioPasos(self, variable):
        self.__promedio_pasos = variable

    @property
    def mortalidad(self):
        return self.__mortalidad
    @mortalidad.setter
    def mortalidad(self, variable):
        self.__mortalidad = variable
