class Vacunas(object):
    """docstring for Vacunas."""

    def __init__(self, tipo, cantidad):
        self.__tipo = tipo
        self.__cantidad_dosis = cantidad
        self.__paso_dado = [0,0]

    @property
    def tipo(self):
        return self.__tipo
    @tipo.setter
    def tipo(self, variable):
        self.__tipo = variable

    @property
    def cantidad(self):
        return self.__cantidad_dosis
    @cantidad.setter
    def cantidad(self, variable):
        self.__cantidad_dosis = variable

    @property
    def paso_1(self):
        return self.__paso_dado[0]
    @paso_1.setter
    def paso_1(self, variable):
        self.__paso_dado[0] = variable
        pass

    @property
    def paso_2(self):
        return self.__paso_dado[1]

    @paso_2.setter
    def paso_2(self, variable):
        self.__paso_dado[1] = variable
