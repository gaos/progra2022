import pandas
import json
from enfermedad import Enfermedad
from comunidad import Comunidad
from simulacion import Simulacion


def main():

    virus_Z = Enfermedad(probabilidadInfeccion=0.3, promedio_pasos=10, mortal=25,
                         gravedad = 5)

    comunidad = Comunidad(num_ciudadanos=500,promedio_conexion_fisica=4,
                          enfermedad=virus_Z,num_infectados=10,
                           probabilidad_contacto_estrecho=0.8, gente_random=4)

    simulador = Simulacion(comunidad, virus_Z)
    simulador.crea_contactos()
    simulador.run(numero_dias=30)

"""
    # Opening JSON file
    f = open('data.json')

    # returns JSON object as
    # a dictionary
    data = json.load(f)

    # Closing file
    f.close()
"""

if __name__ == "__main__":
    main()
