import random as ran
from vacuna import Vacunas
class Ciudadano():
    """docstring for Ciudadano."""

    def __init__(self, id):

        enfermedad_base_posibles = ["asma", "enfermedad cerebrovascular",
                                    "fibrosis quistica", "hipertension"]
        afecciones_posibles = ["obesidad", "desnutricion"]

        self.__id = id
        self.__edad = ran.randint(0,100)
        self.__infectado = False
        self.__estado = True
        self.__contador = 0
        self.__inmune = False
        self.__sano = True
        self.__familia = []
        self.__grave = False
        self.__vacuna = Vacunas(0,0)
        random = ran.randint(0,99) + 1
        if random <= 65 :
            self.__afeccion = ran.choice(afecciones_posibles)
        else:
            self.__afeccion = "No tiene"

        random = ran.randint(0,99) + 1
        if random <= 25 :
            self.__enfermedad_base = ran.choice(enfermedad_base_posibles)
        else:
            self.__enfermedad_base = "No tiene"

    #getter con setter
    @property
    def ID(self):
        return self.__id

    @ID.setter
    def ID(self, variable):
        self.__id = variable

    @property
    def vacuna(self):
        return self.__vacuna

    @vacuna.setter
    def vacuna(self, variable):
        self.__vacuna = variable

    @property
    def grave(self):
        return self.__grave

    @grave.setter
    def grave(self, variable):
        self.__grave = variable

    @property
    def base(self):
        return self.__enfermedad_base

    @base.setter
    def base(self, variable):
        self.__enfermedad_base = variable

    @property
    def afeccion(self):
        return self.__afeccion

    @afeccion.setter
    def afeccion(self, variable):
        self.__afeccion = variable

    @property
    def edad(self):
        return self.__edad

    @edad.setter
    def edad(self, variable):
        self.__edad =variable

    @property
    def infectado(self):
        return self.__infectado

    @infectado.setter
    def infectado(self, variable):
        self.__infectado = variable

    @property
    def estado(self):
        return self.__estado

    @estado.setter
    def estado(self, variable):
        self.__estado = variable

    @property
    def contador(self):
        return self.__contador

    @contador.setter
    def contador(self, variable):
        self.__contador = variable

    @property
    def inmune(self):
        return self.__inmune

    @inmune.setter
    def inmune(self, variable):
        self.__inmune = variable

    @property
    def sano(self):
        return self.__sano

    @sano.setter
    def sano(self, variable):
        self.__sano = variable

    @property
    def familia(self):
        return self.__familia

    def familiaAdd(self, add):
        self.__familia.append(add)
