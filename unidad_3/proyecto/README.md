# Proyecto 1 - Unidad III





Este programa fue escrito en lenguaje de programación Python, y consiste en realizar una simulación de una enfermedad infecciosa en una población de 20 mil ciudadanos, tambien se incorpora un modelo matematico que busca describir el comportamiento de la enfermedad infecciosa. Esta simulación es para 

* POO

* Clases

* Funciones y Metodos







## Historia





Escrito y desarrollado por el estudiante de Ing. Civil en Bioinformática Gabriel Rojas Hormazábal, Cristobal Briceño Arellano y Raimundo Oliva San Feliu utilizando para ello el lenguaje de programación Python.







## Para empezar





Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas.









### Pre-requisitos





Es requisito cerciorarse de tener instalado Python y su compilador correspondiente en el equipo donde ejecutará el proyecto.

El siguiente paso antes de iniciar la instalacion y ejecucion del progra es configurar PyGObject y sus dependencias, para ello existen dos opciones. La primera es instalar el paquete desde el repositorio oficial (PyGObject), casi todas las principales distribuciones de Linux empaquetan versiones recientes de PyGObject. La segunda es instalar PyGObject desde la fuente, usando JHBuild, este está diseñado para compilar fácilmente paquetes fuente y descubrir qué dependencias se deben compilar y en qué orden. Para configurar JHBuild, siga el manual de JHBuild de su repositorio oficial.









### Instalación y ejecución



Para iniciar el programa es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones), posteriormente se debe instalar “git” directamente desde la terminal de su ordenador Linux con el comando “sudo apt install git”. Luego de esto, uno debe proceder a crear su cuenta de GitLab, en caso de ya tener una omitir este paso. Lo siguiente es dirigirse al repositorio < xyxyxyxy > y presionar la opción “Clone” seleccionando el formato https para copiarlo. Después uno debe dirigirse a la terminal y escribir el comando “git clone < xyxyxyxyx >, para finalizar escribir el comando “cd unidad_2/guia_3/” y seguido “python3 start.py”. Tras haber seguido estos pasos deberías poder interactuar con el programa sin mayor problema. Puede subir al repositorio esto para desde gitlab visualizar el archivo markdown en línea.







## Funcionamiento

lol


## Codificación





Soporta la codificación estándar UTF-8.









## Construido con





* [Visual Studio Code.](https://code.visualstudio.com) y [Atom.](https://atom.io) - IDE utilizado para el desarrollo del proyecto.









## Autores





* Gabriel Rojas Hormazábal.

* Cristóbal Briceño Arellano.

* Raimundo Oliva San Feliu
